<?php

namespace MageFan\Frankenstein\Observer;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CheckoutCustomerLogged implements ObserverInterface
{

  /**
   * @var \Magento\Framework\App\Response\RedirectInterface
   */
  protected $redirect;

  /**
   * Customer session
   *
   * @var \Magento\Customer\Model\Session
   */
  protected $_customerSession;

  /**
   * @var \Magento\Framework\App\Request\Http
   */
  protected $request;

 public function __construct(
   \Magento\Framework\App\Request\Http $request,
   \Magento\Customer\Model\Session $customerSession,
   \Magento\Framework\App\Response\RedirectInterface $redirect
 )
 {
   $this->request = $request;
   $this->_customerSession = $customerSession;
   $this->redirect = $redirect;
 }


  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    $moduleName = $this->request->getModuleName();
    $controller = $observer->getControllerAction();
    $openModules = array(
      'cms',
      'contact',
      'customer'
    );
   if (!in_array($moduleName, $openModules))
    {
      $this->redirect->redirect($controller->getResponse(), 'customer/account/login');
    }
    return $this;
  }

}