<?php

namespace MageFan\Frankenstein\Pricing\Render;

use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Catalog\Pricing\Price;
use Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render;
use Magento\Framework\Pricing\Render\PriceBox as BasePriceBox;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Msrp\Pricing\Price\MsrpPrice;

class FinalPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox
{
  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Framework\Pricing\SaleableInterface $saleableItem,
    \Magento\Framework\Pricing\Price\PriceInterface $price,
    \Magento\Framework\Pricing\Render\RendererPool $rendererPool,
    array $data = [],
    \Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface $salableResolver = null,
    \Magento\Catalog\Pricing\Price\MinimalTierPriceCalculator $minimalPriceCalculator = null)
  {

    parent::__construct(
      $context,
      $saleableItem,
      $price,
      $rendererPool,
      $data,
      $salableResolver,
      $minimalPriceCalculator);
  }

  protected function wrapResult($html)
  {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $customerSession = $objectManager->get('Magento\Customer\Model\Session');
    $httpContext = $objectManager->get('Magento\Framework\App\Http\Context');

    if ($customerSession->isLoggedIn())
    {
      $groupId = $customerSession->getCustomer()->getGroupId();

      if ($groupId == 2)
      {
        return '<div class="price-box ' . $this->getData('css_classes') . '" ' .
          'data-role="priceBox" ' .
          'data-product-id="' . $this->getSaleableItem()->getId() . '"' .
          '>' . $html . '</div>';
      }else{
        $message = 'Please Login To See Price';
        return '<div class="price-box ' . $this->getData('css_classes') . '" ' .
          'data-role="priceBox" ' .
          'data-product-id="' . $this->getSaleableItem()->getId() . '"' .
          '>' . $message . '</div>';
      }
      }

  }

}