<?php
namespace MageFan\Frankenstein\Model;

class Frankenstein extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
  const CACHE_TAG = 'magefan_frankenstein';

  protected function _construct()
  {
     $this->_init('MageFan\Frankenstein\Model\ResourceModel\Frankenstein');
  }

  public function getIdentities()
  {
    return [self::CACHE_TAG . '_' . $this->getId()];
  }

  public function create()
  {
    return $this;
  }

}