<?php
namespace MageFan\Frankenstein\Model\ResourceModel;


class Frankenstein extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

  public function __construct(
    \Magento\Framework\Model\ResourceModel\Db\Context $context
  )
  {
    parent::__construct($context);
  }

  protected function _construct()
  {
    $this->_init('magefan_frankenstein', 'title');
  }

}