<?php

namespace VendorName\ModuleName\Cron;

class FrankensteinCronModel
{
  protected $objectManager;
  protected $abstractProductBlock;

  public function __construct(
    \Magento\Catalog\Block\Product\AbstractProduct $block
  )
  {
    $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

    $this->abstractProductBlock = $block->getLayout()->createBlock('\Magento\Catalog\Block\Product\AbstractProduct');
  }

  public function execute()
  {
    $productCollection = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
    $collection = $productCollection->load();

    foreach ($collection as $product) {
      $price =  $this->abstractProductBlock->getProductPrice($product);
      $price = $price+1;
      $product->setPrice($price);
    }
    return $this;
  }
}