<?php

namespace MageFan\Frankenstein\ViewModel;

class Frankenstein implements \Magento\Framework\View\Element\Block\ArgumentInterface
{

  public function getText()
  {
    return 'Frankenstein rise again!';
  }
}