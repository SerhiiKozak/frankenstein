<?php

namespace MageFan\Frankenstein\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
  public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
    $installer = $setup;
    $installer->startSetup();

    /**
     * Create table 'magefan_frankenstein'
     */

    $table = $installer->getConnection()->newTable($installer->getTable('magefan_frankenstein')
    )->addColumn(
      'id',
      \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
      null,
      ['identity' => true, 'nullable' => false, 'primary' =>true],
      'ID'
    )->addColumn(
      'title',
      \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
      255,
      ['nullable' => true],
      'TITLE'
    );

    $installer->getConnection()->createTable($table);
    $installer->endSetup();
  }
}