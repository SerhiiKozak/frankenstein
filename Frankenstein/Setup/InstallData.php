<?php
namespace MageFan\Frankenstein\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
  protected $_exampleFactory;

  public function __construct(\MageFan\Frankenstein\Model\Frankenstein $exampleFactory)
  {
    $this->_exampleFactory = $exampleFactory;
  }

  public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
  {
    $data = [
      'title' => 'Frankenstein',
    ];
    $example = $this->_exampleFactory->create();
    $example->addData($data)->save();
  }
}